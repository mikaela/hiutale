{
  outputs = { self, nixpkgs }: {
    # After the dot comes the hostname
    nixosConfigurations.portable20221213 = nixpkgs.lib.nixosSystem {
      system = "x86_64-linux";
      modules = [
          ./portable20221213-hardware.nix
          ./portable20221213.nix
      ];
    };
  };
}
